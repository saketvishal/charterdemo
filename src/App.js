import React, {Component} from 'react';
import './App.css';
import 'jquery/src/jquery'
import 'jquery-easing/jquery.easing.1.3'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import 'startbootstrap-sb-admin/js/sb-admin'
import 'startbootstrap-sb-admin/css/sb-admin.min.css'
import 'font-awesome/css/font-awesome.min.css';
import AppRouter from './routers/AppRouter';
import {Provider} from 'react-redux';
import configureStore from './store/store.dev'
class App extends Component {
    render() {
        return (<div className="App">
            <Provider store={configureStore()}>
                <AppRouter/></Provider>
        </div>);
    }
}
export default App;
