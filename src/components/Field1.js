import React, {Component} from 'react';
//import PropTypes from 'prop-types';
import VISHALCalculatorForm from './Field1Form';
import VISHALCalculatorGrid from './Grids/Field1Grid';
import {connect} from 'react-redux';
import ErrorBoundary from './ErrorBoundary';
import {RingLoader} from 'react-spinners';
import {
    GetVISHALFIELD1Data,
    GetVISHALFIELD2Data,
    GetAlmMapDsIdsData,
    GetMvbsDsIdsData,
    submitForVISHALCalculation,
    GetVISHALCalcGridData
} from '../actions/VISHALCalcActions'
class VISHALCalculator extends Component {
    componentWillMount() {
        this.props.getYalYq();
        this.props.getGridData();
    }
    render() {
        return (<div>
            <ErrorBoundary error={this.props.error}>
                <h1></h1>
            </ErrorBoundary>
            <VISHALCalculatorForm VISHALFIELD1={this.props.VISHALFIELD1} VISHALFIELD2={this.props.VISHALFIELD2} AlmMapDsIds={this.props.AlmMapDsIds} MvbsDsIds={this.props.MvbsDsIds} VISHALCalcGridData={this.props.VISHALCalcGridData} SubmitCalculation={this.props.submitCalculation} getVISHALFIELD2={this.props.getVISHALFIELD2} getAlmMapDsIds={this.props.getAlmMapDsIds} getMvbsDsIds={this.props.getMvbsDsIds} isLoading={this.props.isLoading}></VISHALCalculatorForm>
            <VISHALCalculatorGrid GridData={this.props.VISHALCalcGridData}></VISHALCalculatorGrid>
        </div>);
    }
}
const mapStateToProps = (state) => {
    return {
        VISHALFIELD1: state.VISHALCalc.VISHALFIELD1,
        VISHALFIELD2: state.VISHALCalc.VISHALFIELD2,
        AlmMapDsIds: state.VISHALCalc.AlmMapDsId,
        MvbsDsIds: state.VISHALCalc.MvbsDsId,
        VISHALCalcGridData: state.VISHALCalc.GridData,
        isLoading: state.VISHALCalc.isLoading,
        error: state.VISHALCalc.error
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getYalYq: () => {
            dispatch(GetVISHALFIELD1Data())
        },
        getVISHALFIELD2: (VISHALFIELD1) => {
            dispatch(GetVISHALFIELD2Data(VISHALFIELD1));
        },
        getAlmMapDsIds: (VISHALFIELD1, VISHALFIELD2) => {
            dispatch(GetAlmMapDsIdsData(VISHALFIELD1, VISHALFIELD2));
        },
        getMvbsDsIds: (VISHALFIELD1, VISHALFIELD2, AlmMapDsIds) => {
            dispatch(GetMvbsDsIdsData(VISHALFIELD1, VISHALFIELD2, AlmMapDsIds));
        },
        getGridData: () => {
            dispatch(GetVISHALCalcGridData());
        },
        submitCalculation: (VISHALFIELD1, VISHALFIELD2, AlmMapDsIds, MvbsDsIds) => {
            dispatch(submitForVISHALCalculation(VISHALFIELD1, VISHALFIELD2, AlmMapDsIds, MvbsDsIds));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(VISHALCalculator);
