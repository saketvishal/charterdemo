import React, {Component} from 'react';
import VISHALField2Form from './VISHALField2Form';
import VishalCalcField2Grid from './Grids/';
import {connect} from 'react-redux';
import ErrorBoundary from './ErrorBoundary';
import {RingLoader} from 'react-spinners';
import {
    GetVISHALFIELD1Data,
    GetVISHALFIELD2Data,
    submitForAssetsCalculation,
    submitForGFSCalculation,
    submitForPVCalculation,
    GetMVCalcGridData
} from '../actions/Field2Actions'
class VishalCalcField2 extends Component {
    static defaultProps = {}
    static propTypes = {}
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentWillMount() {
        this.props.getYalYq();
        this.props.getGridData();
    }
    render() {
        return (
            this.props.isLoading
            ? <div className="outPopUp">
                <RingLoader color={'#123abc'} loading={this.props.isLoading}></RingLoader>
            </div>
            : <div>
                <ErrorBoundary error={this.props.error}>
                    <h1></h1>
                </ErrorBoundary>
                <VISHALField2Form VISHALFIELD1={this.props.VISHALFIELD1} VISHALFIELD2={this.props.VISHALFIELD2} SubmitCalculation={this.props.submitCalculation} getVISHALFIELD2={this.props.getVISHALFIELD2}/>
                <VishalCalcField2Grid GridData={this.props.VISHALCalcGridData}></VishalCalcField2Grid >
            </div>);
    }
}
const mapStateToProps = (state) => {
    return {VISHALFIELD1: state.MVCalc.VISHALFIELD1, VISHALFIELD2: state.MVCalc.VISHALFIELD2, VISHALCalcGridData: state.MVCalc.GridData, isLoading: state.MVCalc.isLoading, error: state.MVCalc.error};
};
const mapDispatchToProps = (dispatch) => {
    return {
        getYalYq: () => {
            dispatch(GetVISHALFIELD1Data())
        },
        getVISHALFIELD2: (VISHALFIELD1) => {
            dispatch(GetVISHALFIELD2Data(VISHALFIELD1));
        },
        getGridData: () => {
            dispatch(GetMVCalcGridData());
        },
        submitForAssetsCalculation: (VISHALFIELD1, VISHALFIELD2) => {
            dispatch(submitForAssetsCalculation(VISHALFIELD1, VISHALFIELD2));
        },
        submitForGFSCalculation: (VISHALFIELD1, VISHALFIELD2) => {
            dispatch(submitForGFSCalculation(VISHALFIELD1, VISHALFIELD2));
        },
        submitForPVCalculation: (VISHALFIELD1, VISHALFIELD2) => {
            dispatch(submitForPVCalculation(VISHALFIELD1, VISHALFIELD2));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(VishalCalcField2);