import React, {Component} from 'react';
import PropTypes from 'prop-types';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import matchSorter from 'match-sorter'
export default class VishalCalcField2Grid extends Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        GridData: PropTypes.array,
        isLoading: PropTypes.bool
    }
    static defaultProps = {
        GridData: [],
        isLoading: false
    }
    render() {
        return (<div>
            <ReactTable data={this.props.GridData} filterable={true} columns={[
                    {
                        Header: "Calculation Id",
                        accessor: "CALCULATIONID"
                    }, {
                        Header: "Created By",
                        id: "CREATEDBY",
                        accessor: d => d.CREATEDBY
                    }, {
                        Header: "Start Date",
                        id: "STARTDATE",
                        accessor: d => d.STARTDATE
                    }, {
                        Header: "Status",
                        id: "STATUS",
                        accessor: d => d.STATUS
                    }, {
                        Header: "Dataset Id",
                        id: "VISHALFIELD2",
                        accessor: d => d.VISHALFIELD2
                    }, {
                        Header: "VISHALFIELD1",
                        id: "VALYM",
                        accessor: d => d.VALYM
                    }
                ]} defaultPageSize={10} className="-striped -highlight"/>
        </div>);
    }
}
