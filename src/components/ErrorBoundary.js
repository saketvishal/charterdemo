import React from 'react';
export default class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (this.props.error) {
            // Error path
            return (<div>
                <h2>Something went wrong.</h2>
                <details style={{
                        whiteSpace: 'pre-wrap'
                    }}>
                    {
                        this.props.error && this
                            .props
                            .error
                            .toString()
                    }
                </details>
            </div>);
        }
        // Normally, just render children
        return this.props.children;
    }
}
