import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
class VISHALField2Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedVISHALFIELD1: '',
            selectedVISHALFIELD2: ''
        };
        this.handelSubmitMVClick = this
            .handelSubmitMVClick
            .bind(this);
        this.handelVISHALFIELD1Change = this
            .handelVISHALFIELD1Change
            .bind(this);
        this.handelVISHALFIELD2Change = this
            .handelVISHALFIELD2Change
            .bind(this);
    }
    static propTypes = {
        VISHALFIELD2: PropTypes.number,
        valYm: PropTypes.number
    }
    static defaultProps = {
        VISHALFIELD2: 0,
        valYm: 0
    }
    handelVISHALFIELD1Change(value) {
        this.setState((prevState, props) => {
            return {selectedVISHALFIELD1: value}
        });
        this
            .props
            .getVISHALFIELD2(value);
    }
    handelVISHALFIELD2Change(value) {
        this.setState((prevState, props) => {
            return {selectedVISHALFIELD2: value}
        });
        this
            .props
            .getAlmMapDsIds(this.state.selectedVISHALFIELD1, value);
    }
    handelSubmitMVClick(e) {
        e.preventDefault();
        this
            .props
            .SubmitCalculation(this.state.selectedVISHALFIELD1, this.state.selectedVISHALFIELD2)
    }
    render() {
        return (<form className="form-style-7" onSubmit={this.handelSubmitMVClick}>
            <ul>
                <li>
                    <label htmlFor="name">Val-Yq</label>
                    <Select id="VISHALFIELD1" ref="VISHALFIELD1" autoFocus="autoFocus" options={this.props.valYm} name="VISHALFIELD1" value={this.state.selectedVISHALFIELD1} onChange={this.VISHALFIELD1Change}/>
                    <span>type a year for val-ym</span>
                </li>
                <li>
                    <label htmlFor="name">VISHALFIELD2</label>
                    <Select id="VISHALFIELD2" ref="VISHALFIELD2" autoFocus="autoFocus" options={this.props.VISHALFIELD2} name="VISHALFIELD2" value={this.state.selectedVISHALFIELD2}/>
                    <span>type a value for VISHALFIELD2</span>
                </li>
                <li></li>
            </ul>
            <div>
                <input type="checkbox" name="checkbox" id="AssetsCalculatorId" value="value"/>
                <label htmlFor="AssetsCalculatorId">Assets Calculator</label><br/>
                <input type="checkbox" name="checkbox" id="PVCalculatorId" value="value"/>
                <label htmlFor="PVCalculatorId">PV Calculator</label><br/>
                <input type="checkbox" name="checkbox" id="GFSCalculatorId" value="value"/>
                <label htmlFor="GFSCalculatorId">GFS Calculator</label>
            </div><br/>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>);
    }
}
export default VISHALField2Form;
