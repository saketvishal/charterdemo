import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import {RingLoader} from 'react-spinners';
class VISHALCalculatorForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedVISHALFIELD1: '',
            selectedVISHALFIELD2: '',
            selectedAlmMapDsIds: '',
            selectedMvbsDsIds: ''
        };
        this.handelSubmitVISHALClick = this
            .handelSubmitVISHALClick
            .bind(this);
        this.handelVISHALFIELD1Change = this
            .handelVISHALFIELD1Change
            .bind(this);
        this.handelVISHALFIELD2Change = this
            .handelVISHALFIELD2Change
            .bind(this);
        this.handelAlmMapDsIdChange = this
            .handelAlmMapDsIdChange
            .bind(this);
        this.handelMvbsDsIDChange = this
            .handelMvbsDsIDChange
            .bind(this);
    }
    static propTypes = {
        VISHALFIELD2: PropTypes.array,
        VISHALFIELD1: PropTypes.array,
        AlmMapDsIds: PropTypes.array,
        MvbsDsIds: PropTypes.array,
        isLoading: PropTypes.bool
    }
    static defaultProps = {
        VISHALFIELD2: [],
        VISHALFIELD1: [],
        AlmMapDsIds: [],
        MvbsDsIds: [],
        isLoading: false
    }
    handelVISHALFIELD1Change(value) {
        this.setState((prevState, props) => {
            return {selectedVISHALFIELD1: value}
        });
        this
            .props
            .getVISHALFIELD2(value);
    }
    handelVISHALFIELD2Change(value) {
        this.setState((prevState, props) => {
            return {selectedVISHALFIELD2: value}
        });
        this
            .props
            .getAlmMapDsIds(this.state.selectedVISHALFIELD1, value);
    }
    handelAlmMapDsIdChange(value) {
        this.setState((prevState, props) => {
            return {selectedAlmMapDsIds: value}
        });
        this
            .props
            .getMvbsDsIds(this.state.selectedVISHALFIELD1, this.state.selectedVISHALFIELD2, value);
    }
    handelMvbsDsIDChange(value) {
        this.setState((prevState, props) => {
            return {selectedMvbsDsIds: value}
        });
    }
    handelSubmitVISHALClick(e) {
        e.preventDefault();
        this
            .props
            .SubmitCalculation(this.state.selectedVISHALFIELD1, this.state.selectedVISHALFIELD2, this.state.selectedAlmMapDsIds, this.state.selectedMvbsDsIds)
    }
    render() {
        return (
            this.props.isLoading
            ? <div className="outPopUp">
                <RingLoader color={'#123abc'} loading={this.props.isLoading}></RingLoader>
            </div>
            : <form className="form-style-7" onSubmit={this.handelSubmitVISHALClick}>
                <ul>
                    <li>
                        <label htmlFor="name">VAL_YQ</label>
                        <Select id="VISHALFIELD1" ref="VISHALFIELD1" autoFocus="autoFocus" options={this.props.VISHALFIELD1} name="VISHALFIELD1" value={this.state.selectedVISHALFIELD1} onChange={this.handelVISHALFIELD1Change}/>
                        <span>type a year for val-ym</span>
                    </li>
                    <li>
                        <label htmlFor="name">Data Set ID</label>
                        <Select id="VISHALFIELD2" ref="VISHALFIELD2" autoFocus="autoFocus" options={this.props.VISHALFIELD2} name="VISHALFIELD2" value={this.state.selectedVISHALFIELD2} onChange={this.handelVISHALFIELD2Change}/>
                        <span>type a value for VISHALFIELD2</span>
                    </li>
                    <li>
                        <label htmlFor="name">ALM Mapping Data Set ID</label>
                        <Select id="AlmMapDsIds" ref="AlmMapDsIds" autoFocus="autoFocus" options={this.props.AlmMapDsIds} name="AlmMapDsIds" value={this.state.selectedAlmMapDsIds} onChange={this.handelAlmMapDsIdChange}/>
                        <span>type a value for AlmMapDsIds</span>
                    </li>
                    <li>
                        <label htmlFor="name">MVBS Data Set ID</label>
                        <Select id="MvbsDsIds" ref="MvbsDsIds" autoFocus="autoFocus" options={this.props.MvbsDsIds} name="MvbsDsIds" value={this.state.selectedMvbsDsIds} onChange={this.handelMvbsDsIDChange}/>
                        <span>type a value for MvbsDsIds</span>
                    </li>
                    <li></li>
                </ul>
                <button className="btn btn-primary">Submit</button>
            </form>);
    }
}
export default VISHALCalculatorForm;
