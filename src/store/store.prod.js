import {createStore, applyMiddleware} from 'redux'
import ReduxThunk from 'redux-thunk'
import ReduxPromise from 'redux-promise';
import rootReducer from '../Reducers/index'
import initialState from '../state/index';
const middlewares = [ReduxThunk]
const enhancer = [applyMiddleware(...middlewares)]
export default function configureStore(defaultState = initialState) {
    return createStore(rootReducer, defaultState, ...enhancer)
}
