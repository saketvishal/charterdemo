import {createStore, applyMiddleware} from 'redux'
import ReduxThunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware';
import {composeWithDevTools} from 'redux-devtools-extension';
import rootReducer from '../Reducers/index';
import initialState from '../state/index';
import logger from 'redux-logger';
export default function configureStore(defaultState = initialState) {
    const middlewares = [promiseMiddleware(), ReduxThunk, logger]
    const enhancers = [applyMiddleware(...middlewares)]
    const composeEnhancers = composeWithDevTools({})
    const store = createStore(rootReducer, defaultState, composeEnhancers(...enhancers))
    return store
}
