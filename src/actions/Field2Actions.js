import Field2ActionConstants from '../Constants/Field2ActionConstants';
import axios from 'axios';
export function GetVISHALFIELD1Data() {
    return function (dispatch) {
        dispatch({type: Field2ActionConstants.GET_VISHALMDEMO_VISHALMFIELD1Q_, payload: axios.get('DEMOURL/VISHALFIELD1')})
    }
}
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
        }
    return true;
}
export function GetVISHALFIELD2Data(VISHALFIELD1) {
    if (!isEmpty(VISHALFIELD1)) {
        VISHALFIELD1 = VISHALFIELD1.value;
        return function (dispatch) {
            dispatch({
                type: Field2ActionConstants.GET_VISHALMDEMO_DATA_SET_ID,
                payload: axios.get('DEMOURL/GETVISHALDFIELD3?VISHALFIELD1=' + VISHALFIELD1)
            })
        }
    }
}
export function submitForAssetsCalculation(VISHALFIELD1, VISHALFIELD2) {
    const VISHALFIELD11 = VISHALFIELD1.value;
    const VISHALFIELD21 = VISHALFIELD2.value;
    return function (dispatch) {
        dispatch({
            type: Field2ActionConstants.SUBMIT_VISHALMDEMO_ASSETS_CALC,
            payload: axios.get('hDEMOURL/VISHALCalculator/rest/StartVISHALEngine?VISHALFIELD2=' + VISHALFIELD21 + '&VAL_YQ=' + VISHALFIELD11 + "&UserName=USERNAME")
        })
    }
}
export function submitForGFSCalculation(VISHALFIELD1, VISHALFIELD2) {
    const VISHALFIELD11 = VISHALFIELD1.value;
    const VISHALFIELD21 = VISHALFIELD2.value;
    return function (dispatch) {
        dispatch({
            type: Field2ActionConstants.SUBMIT_VISHALMDEMO_GFS_CALC,
            payload: axios.get('hDEMOURL/VISHALCalculator/rest/StartVISHALEngine?VISHALFIELD2=' + VISHALFIELD21 + '&VAL_YQ=' + VISHALFIELD11 + "&UserName=USERNAME")
        })
    }
}
export function submitForPVCalculation(VISHALFIELD1, VISHALFIELD2) {
    const VISHALFIELD11 = VISHALFIELD1.value;
    const VISHALFIELD21 = VISHALFIELD2.value;
    return function (dispatch) {
        dispatch({
            type: Field2ActionConstants.SUBMIT_VISHALMDEMO_PV_CALC,
            payload: axios.get('hDEMOURL/VISHALCalculator/rest/StartVISHALEngine?VISHALFIELD2=' + VISHALFIELD21 + '&VAL_YQ=' + VISHALFIELD11 + "&UserName=USERNAME")
        })
    }
}
export function GetMVCalcGridData() {
    return function (dispatch) {
        dispatch({type: Field2ActionConstants.SUBMIT_VISHALMDEMO_PV_CALC, payload: axios.get('hDEMOURL/VISHALCalculator/rest/getVISHALHistory')})
    }
}
