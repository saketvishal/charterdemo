import VISHALActionConstants from '../Constants/VISHALActionConstants';
import axios from 'axios';
import URLConstants from '../Constants/UrlConstants'
import VISHALMethodConstants from '../Constants/VISHALMethodConstants'
export function GetVISHALFIELD1Data() {
    return function(dispatch) {
        dispatch({
            type: VISHALActionConstants.GET_VISHALMFIELD1Q_,
            payload: axios.get(`${URLConstants.VISHAL_TEMP_DATA_SERVICE}/${VISHALMethodConstants.GETVISHALDFIELD1}`)
        })
    }
}
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
        }
    return true;
}
export function GetVISHALFIELD2Data(VISHALFIELD1) {
    if (!isEmpty(VISHALFIELD1)) {
        VISHALFIELD1 = VISHALFIELD1.value;
        return function(dispatch) {
            dispatch({
                type: VISHALActionConstants.GET_DATA_SET_ID,
                payload: axios.get(`${URLConstants.VISHAL_TEMP_DATA_SERVICE}/${VISHALMethodConstants.GETVISHALDFIELD3}?VISHALFIELD1=${VISHALFIELD1}`)
            })
        }
    }
}
export function GetAlmMapDsIdsData(VISHALFIELD1, dsid) {
    console.log(VISHALFIELD1 + "VISHALFIELD1");
    console.log(dsid + "dsid");
    if (!isEmpty(VISHALFIELD1) && !isEmpty(dsid)) {
        VISHALFIELD1 = VISHALFIELD1.value;
        dsid = dsid.value;
        console.log(`${URLConstants.VISHAL_TEMP_DATA_SERVICE}/${VISHALMethodConstants.GETVISHALDFIELD2}?VISHALFIELD1=${VISHALFIELD1}&dsid=${dsid}`);
        return function(dispatch) {
            dispatch({
                type: VISHALActionConstants._VISHALDFIELD3_ID,
                payload: axios.get(`${URLConstants.VISHAL_TEMP_DATA_SERVICE}/${VISHALMethodConstants.GETVISHALDFIELD2}?VISHALFIELD1=${VISHALFIELD1}&dsid=${dsid}`)
            })
        }
    }
}
export function GetMvbsDsIdsData(VISHALFIELD1, dsid, AlmMapDsId) {
    if (!isEmpty(VISHALFIELD1) && !isEmpty(dsid) && !isEmpty(AlmMapDsId)) {
        VISHALFIELD1 = VISHALFIELD1.value;
        dsid = dsid.value;
        AlmMapDsId = AlmMapDsId.value;
        return function(dispatch) {
            dispatch({
                type: VISHALActionConstants.GET_VISHALDFIELD2_ID,
                payload: axios.get(`${URLConstants.VISHAL_TEMP_DATA_SERVICE}/${VISHALMethodConstants.GETVISHALDFIELD4}?VISHALFIELD1=${VISHALFIELD1}&dsid=${dsid}&almdsid=${AlmMapDsId}`)
            })
        }
    }
}
export function submitForVISHALCalculation(VISHALFIELD1, VISHALFIELD2, AlmMapDsIds, MvbsDsIds) {
    const VISHALFIELD11 = VISHALFIELD1.value;
    const VISHALFIELD21 = VISHALFIELD2.value;
    return function(dispatch) {
        dispatch({
            type: VISHALActionConstants.SUBMIT_VISHAL_CALC_ID,
            payload: axios.get('hDEMOURL/VISHALCalculator/rest/StartVISHALEngine?VISHALFIELD2=' + VISHALFIELD21 + '&VAL_YQ=' + VISHALFIELD11 + "&UserName=USERNAME")
        })
    }
}
export function GetVISHALCalcGridData() {
    return function(dispatch) {
        dispatch({
            type: VISHALActionConstants.GET_VISHAL_GRID_DATA,
            payload: axios.get(`${URLConstants.VISHAL_TEMP_DATA_SERVICE}/${VISHALMethodConstants.VISHALCALCULATORGRIDDATA}`)
        })
    }
}
