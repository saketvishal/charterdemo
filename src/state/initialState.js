// export {
//     default as initialState
// }
// from './initialState'
const initialState = {
    VISHALCalc: {
        VISHALFIELD1: [],
        VISHALFIELD2: [],
        AlmMapDsId: [],
        MvbsDsId: [],
        Grid: [],
        isSubmitActive: false,
        isLoading: false,
        error: null
    },
    MVCalc: {
        VISHALFIELD1: [],
        VISHALFIELD2: [],
        AssetCalc: false,
        PVCalc: false,
        GFSCalc: false,
        Grid: [],
        isSubmitActive: false,
        isLoading: false,
        error: null
    }
};
export default initialState;
export default initialState;
