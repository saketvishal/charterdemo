// export {
//     default as initialState
// }
// from './initialState'
const initialState = {
    VISHALCalc: {
        VISHALFIELD1: [],
        VISHALFIELD2: [],
        AlmMapDsId: [],
        MvbsDsId: [],
        GridData: [],
        isSubmitActive: false,
        isLoading: false,
        error: null
    },
    MVCalc: {
        VISHALFIELD1: [],
        VISHALFIELD2: [],
        AssetCalc: false,
        PVCalc: false,
        GFSCalc: false,
        GridData: [],
        isSubmitActive: false,
        isLoading: false,
        error: null
    }
};
export default initialState;
