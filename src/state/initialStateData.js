const initialState = {
    UI: {
        VISHALCalc: {
            VISHALFIELD1: [
                '201701',
                '201702',
                '201703',
                '201704',
                '201705',
                '201706',
                '201707'
            ],
            VISHALFIELD2: [
                {
                    value: 10,
                    description: 'val1 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val2 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val3 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val4 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val5 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val6 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val7 VISHALFIELD2 description'
                }
            ],
            AlmMapDsId: [
                {
                    value: 10,
                    description: 'val1 AlmMapDsId description'
                }, {
                    value: 10,
                    description: 'val2 AlmMapDsId description'
                }, {
                    value: 10,
                    description: 'val3 AlmMapDsId description'
                }, {
                    value: 10,
                    description: 'val4 AlmMapDsId description'
                }, {
                    value: 10,
                    description: 'val5 AlmMapDsId description'
                }, {
                    value: 10,
                    description: 'val6 AlmMapDsId description'
                }, {
                    value: 10,
                    description: 'val7 AlmMapDsId description'
                }
            ],
            MvbsDsId: [
                {
                    value: 10,
                    description: 'val1 MvbsDsId description'
                }, {
                    value: 10,
                    description: 'val2 MvbsDsId description'
                }, {
                    value: 10,
                    description: 'val3 MvbsDsId description'
                }, {
                    value: 10,
                    description: 'val4 MvbsDsId description'
                }, {
                    value: 10,
                    description: 'val5 MvbsDsId description'
                }, {
                    value: 10,
                    description: 'val6 MvbsDsId description'
                }, {
                    value: 10,
                    description: 'val7 MvbsDsId description'
                }
            ],
            Grid: [
                {
                    CalcId: '1',
                    StartDate: '1/1/2017',
                    Status: 'In Progress',
                    DsId: '12',
                    VISHALFIELD1: 201711
                }, {
                    CalcId: '2',
                    StartDate: '1/1/2017',
                    Status: 'In Progress',
                    DsId: '12',
                    VISHALFIELD1: 201711
                }
            ]
        },
        MVCalc: {
            VISHALFIELD1: [
                '201701',
                '201702',
                '201703',
                '201704',
                '201705',
                '201706',
                '201707'
            ],
            VISHALFIELD2: [
                {
                    value: 10,
                    description: 'val1 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val2 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val3 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val4 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val5 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val6 VISHALFIELD2 description'
                }, {
                    value: 10,
                    description: 'val7 VISHALFIELD2 description'
                }
            ],
            AssetCalc: false,
            PVCalc: false,
            GFSCalc: false,
            Grid: [{}]
        }
    },
    isLoading: false
};
export default initialState;
