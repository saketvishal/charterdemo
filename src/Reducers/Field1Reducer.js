import VISHALActionConstants from '../Constants/VISHALActionConstants'
import initialState from '../state/index';
export default function VISHALCalculatorReducer(state = initialState, action) {
    switch (action.type) {
        case VISHALActionConstants.GET_VISHALMFIELD1Q__PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case VISHALActionConstants.GET_VISHALMFIELD1Q__REJECTED:
            console.log('GET_VISHALMFIELD1Q__REJECTED')
            return {
                ...state,
                VISHALCalc: {
                    ...state.VISHALCalc,
                    VISHALFIELD1: []
                },
                error: action.payload,
                isLoading: false
            };
        case VISHALActionConstants.GET_VISHALMFIELD1Q__FULFILLED:
            //    console.log(action.payload.json().);
            return {
                ...state,
                VISHALFIELD1: action
                    .payload
                    .data
                    .map(key => ({
                        value: JSON.stringify(key.VAL_YQ),
                        label: JSON.stringify(key.VAL_YQ)
                    })),
                error: null,
                isLoading: false
            };
        case VISHALActionConstants.GET_DATA_SET_ID_PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case VISHALActionConstants.GET_DATA_SET_ID_REJECTED:
            return {
                ...state,
                VISHALCalc: {
                    ...state.VISHALCalc,
                    VISHALFIELD2: []
                },
                error: action.payload,
                isLoading: false
            };
        case VISHALActionConstants.GET_DATA_SET_ID_FULFILLED:
            return {
                ...state,
                error: null,
                VISHALFIELD2: action
                    .payload
                    .data
                    .map(key => ({
                        value: JSON.stringify(key.VISHALFIELD2),
                        label: JSON.stringify(key.VISHALFIELD2)
                    })),
                isLoading: false
            };
        case VISHALActionConstants.GET_VISHALDFIELD2_ID_PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case VISHALActionConstants.GET_VISHALDFIELD2_ID_REJECTED:
            return {
                ...state,
                VISHALCalc: {
                    ...state.VISHALCalc,
                    MvbsDsId: []
                },
                error: action.payload,
                isLoading: false
            };
        case VISHALActionConstants.GET_VISHALDFIELD2_ID_FULFILLED:
            console.log(action.payload.data);
            return {
                ...state,
                MvbsDsId: action
                    .payload
                    .data
                    .map(key => ({
                        value: JSON.stringify(key.MVBS_DSID),
                        label: JSON.stringify(key.MVBS_DSID)
                    })),
                error: null,
                isLoading: false
            };
        case VISHALActionConstants._VISHALDFIELD3_ID_PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case VISHALActionConstants._VISHALDFIELD3_ID_REJECTED:
            return {
                ...state,
                VISHALCalc: {
                    ...state.VISHALCalc,
                    AlmMapDsId: []
                },
                error: action.payload,
                isLoading: false
            };
        case VISHALActionConstants._VISHALDFIELD3_ID_FULFILLED:
            console.log(action.payload.data);
            return {
                ...state,
                AlmMapDsId: action
                    .payload
                    .data
                    .map(key => ({
                        value: JSON.stringify(key._VISHALDFIELD3ID),
                        label: JSON.stringify(key._VISHALDFIELD3ID)
                    })),
                error: null,
                isLoading: false
            };
        case VISHALActionConstants.GET_VISHAL_GRID_DATA_PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case VISHALActionConstants.GET_VISHAL_GRID_DATA_REJECTED:
            return {
                ...state,
                VISHALCalc: {
                    ...state.VISHALCalc,
                    GridData: []
                },
                error: action.payload,
                isLoading: false
            };
        case VISHALActionConstants.GET_VISHAL_GRID_DATA_FULFILLED:
            return {
                ...state,
                GridData: action.payload.data,
                // GridData: [
                //     {
                //         CalcId: 1,
                //         StartDate: '11/10/2017',
                //         Status: 'Inprocess',
                //         DsId: '1',
                //         VISHALFIELD1: '201708'
                //     }, {
                //         CalcId: 2,
                //         StartDate: '11/1/2017',
                //         Status: 'Complete',
                //         DsId: '1',
                //         VISHALFIELD1: '201708'
                //     }
                // ],
                error: null,
                isLoading: false
            };
        case VISHALActionConstants.SUBMIT_VISHAL_CALC_ID_PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case VISHALActionConstants.SUBMIT_VISHAL_CALC_ID_REJECTED:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        case VISHALActionConstants.SUBMIT_VISHAL_CALC_ID_FULFILLED:
            return {
                ...state,
                isLoading: false,
                error: null
            };
        default:
            return state
    }
}
