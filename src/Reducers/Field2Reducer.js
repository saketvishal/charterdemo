import initialState from '../state/index';
import Field2ActionConstants, {PENDING, FULFILLED, REJECTED} from '../Constants/Field2ActionConstants'
export default function VishalCalcField2Reducer(state = initialState, action) {
    switch (action.type) {
        case Field2ActionConstants.GET_VISHALMDEMO_VISHALMFIELD1Q_ + Field2ActionConstants.PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case Field2ActionConstants.GET_VISHALMDEMO_VISHALMFIELD1Q_ + Field2ActionConstants.REJECTED:
            console.log('GET_VISHALMFIELD1Q__REJECTED')
            return {
                ...state,
                MVCalc: {
                    ...state.MVCalc,
                    VISHALFIELD1: []
                },
                error: action.payload,
                isLoading: false
            };
        case Field2ActionConstants.GET_VISHALMDEMO_VISHALMFIELD1Q_ + Field2ActionConstants.FULFILLED:
            return {
                ...state,
                VISHALFIELD1: action
                    .payload
                    .data
                    .map(key => ({
                        value: JSON.stringify(key.VAL_YQ),
                        label: JSON.stringify(key.VAL_YQ)
                    })),
                error: null,
                isLoading: false
            };
        case Field2ActionConstants.GET_VISHALMDEMO_DATA_SET_ID + Field2ActionConstants.PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case Field2ActionConstants.GET_VISHALMDEMO_DATA_SET_ID + Field2ActionConstants.REJECTED:
            return {
                ...state,
                MVCalc: {
                    ...state.MVCalc,
                    VISHALFIELD2: []
                },
                error: action.payload,
                isLoading: false
            };
        case Field2ActionConstants.GET_VISHALMDEMO_DATA_SET_ID + Field2ActionConstants.FULFILLED:
            return {
                ...state,
                error: null,
                VISHALFIELD2: action
                    .payload
                    .data
                    .map(key => ({
                        value: JSON.stringify(key.VISHALFIELD2),
                        label: JSON.stringify(key.VISHALFIELD2)
                    })),
                isLoading: false
            };
        case Field2ActionConstants.GET_VISHALMDEMO_GRID_DATA + Field2ActionConstants.PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case Field2ActionConstants.GET_VISHALMDEMO_GRID_DATA + Field2ActionConstants.REJECTED:
            return {
                ...state,
                MVCalc: {
                    ...state.MVCalc,
                    GridData: []
                },
                error: action.payload,
                isLoading: false
            };
        case Field2ActionConstants.GET_VISHALMDEMO_GRID_DATA + Field2ActionConstants.FULFILLED:
            return {
                ...state,
                //GridData: action.payload.data,
                GridData: [
                    {
                        CalcId: 1,
                        StartDate: '11/10/2017',
                        Status: 'Inprocess',
                        DsId: '1',
                        VISHALFIELD1: '201708'
                    }, {
                        CalcId: 2,
                        StartDate: '11/1/2017',
                        Status: 'Complete',
                        DsId: '1',
                        VISHALFIELD1: '201708'
                    }
                ],
                error: null,
                isLoading: false
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_ASSETS_CALC + Field2ActionConstants.PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_ASSETS_CALC + Field2ActionConstants.REJECTED:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_ASSETS_CALC + Field2ActionConstants.FULFILLED:
            return {
                ...state,
                isLoading: false,
                error: null
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_GFS_CALC + Field2ActionConstants.PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_GFS_CALC + Field2ActionConstants.REJECTED:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_GFS_CALC + Field2ActionConstants.FULFILLED:
            return {
                ...state,
                isLoading: false,
                error: null
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_PV_CALC + Field2ActionConstants.PENDING:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_PV_CALC + Field2ActionConstants.REJECTED:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        case Field2ActionConstants.SUBMIT_VISHALMDEMO_PV_CALC + Field2ActionConstants.FULFILLED:
            return {
                ...state,
                isLoading: false,
                error: null
            };
        default:
            return state
    }
}
