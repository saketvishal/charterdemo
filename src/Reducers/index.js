import {combineReducers} from 'redux';
import VISHALCalculatorReducer from './VISHALCalcReducer';
import VishalCalcField2Reducer from './MVCalcReducer';
const rootReducer = combineReducers({VISHALCalc: VISHALCalculatorReducer, MVCalc: VishalCalcField2Reducer})
export default rootReducer
