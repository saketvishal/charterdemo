import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import VISHALCalculator from '../components/VISHALCalculator'
import Dashboard from '../components/Dashboard'
import LoadHistory from '../components/LoadHistory'
import VishalCalcField2 from '../components/VishalCalcField2'
import VISHALReport from '../components/VISHALReport'
import LeftNavLinks from './LeftNavLinks'
import NotFoundPage from '../components/NotFoundPage';
const AppRouter = () => (<BrowserRouter>
    <div>
        <LeftNavLinks/>
        <div className="content-wrapper">
            <div className="container-fluid">
                <Switch >
                    <Route path="/" component={Dashboard} exact={true}/>
                    <Route path="/VISHALCalc" component={VISHALCalculator}/>
                    <Route path="/Field2Calc" component={VishalCalcField2}/>
                    <Route path="/LoadHistory" component={LoadHistory}/>
                    <Route component={NotFoundPage}/>
                </Switch>
            </div>
            <footer className="sticky-footer">
                <div className="container">
                    <div className="text-center">
                        <small>Copyright Vishal Singh 2017</small>
                    </div>
                </div>
            </footer>
            {/* Scroll to Top Button */}
            <a className="scroll-to-top rounded" href="#page-top">
                <i className="fa fa-angle-up"></i>
            </a>
        </div>
    </div>
</BrowserRouter>)
export default AppRouter;
