import React from 'react';
import {Link} from 'react-router-dom';
const LeftNavLinks = () => (<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <Link to='/' className="navbar-brand">VISHAL Admin UI</Link>
    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"/>
    </button>
    <div className="collapse navbar-collapse" id="navbarResponsive">
        <ul className="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <Link to='/' className="nav-link">
                    <i className="fa fa-fw fa-dashboard"/>
                    <span className="nav-link-text">Dashboard</span>
                </Link>
            </li>
            <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <Link to='/MVCalc' className="nav-link">
                    <i className="fa fa-fw fa-calculator"/>
                    <span className="nav-link-text">MV Calculator</span>
                </Link>
            </li>
            <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <Link to='/VISHALCalc' className="nav-link">
                    <i className="fa fa-fw fa-calculator"/>
                    <span className="nav-link-text">VISHAL Calculator</span>
                </Link>
            </li>
            <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <Link to='/VISHALReport' className="nav-link">
                    <i className="fa fa-fw fa-file-text"/>
                    <span className="nav-link-text">VISHAL Reporting</span>
                </Link>
            </li>
            <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <Link to='/LoadHistory' className="nav-link">
                    <i className="fa fa-fw fa-history"/>
                    <span className="nav-link-text">Load History</span>
                </Link>
            </li>
        </ul>
        <ul className="navbar-nav sidenav-toggler">
            <li className="nav-item">
                <a className="nav-link text-center" id="sidenavToggler">
                    <i className="fa fa-fw fa-angle-left"/>
                </a>
            </li>
        </ul>
    </div>
</nav>)
export default LeftNavLinks;
